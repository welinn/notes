監控 cpu 使用狀態，輸出檔名範例： `isolate-0x35b29e0-61604-v8.log`
```
node --prof test.js

```

更改 log 路徑及名稱
```
node --prof --logfile=/path/for/log/foo.log --no-logfile-per-isolate test.js
```

將 log 轉成文字檔
```
node --prof-process isolate-0x35b29e0-61604-v8.log > 61604.txt
```

更新 npm 版本
```
sudo npm install npm -g
```

更新 node 到最新穩定版
```
sudo npm cache clean -f
sudo npm install -g n
sudo n stable
```
