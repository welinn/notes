# Deeplearning for mobile

* [Pytorch to ONNX](#pytorch-to-onnx)
* [Keras to TensorFlow Lite](#keras-to-tensorflow-lite)
* [Yolo3 to TensorFlow Lite](#yolo3-to-tensorflow-lite)


## Pytorch to ONNX

來自[這裡](https://docs.aws.amazon.com/zh_tw/dlami/latest/devguide/tutorial-onnx-pytorch-mxnet.html)的範例。

```python
# Build a Mock Model in PyTorch with a convolution and a reduceMean layer
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from torch.autograd import Variable
import torch.onnx as torch_onnx

class Model(nn.Module):
    def __init__(self):
        super(Model, self).__init__()
        self.conv = nn.Conv2d(in_channels=3, out_channels=32, kernel_size=(3,3), stride=1, padding=0, bias=False)

    def forward(self, inputs):
        x = self.conv(inputs)
        #x = x.view(x.size()[0], x.size()[1], -1)
        return torch.mean(x, dim=2)

# Use this an input trace to serialize the model
input_shape = (3, 100, 100)
model_onnx_path = "torch_model.onnx"
model = Model()
model.train(False)

# Export the model to an ONNX file
dummy_input = Variable(torch.randn(1, *input_shape))
output = torch_onnx.export(model, 
                          dummy_input, 
                          model_onnx_path, 
                          verbose=False)
print("Export of torch_model.onnx complete!")
```

當 model 架構簡單時可以直接轉換，但若有多個不同的模型嫁接，以下此行:

```python
output = torch_onnx.export(model, dummy_input, model_onnx_path, verbose=False)
```

會得到 error:
```
TypeError: forward() missing 1 required positional argument: 'text'
```

因為此時模型有隱藏層的緣故，需要將隱藏層的 input 也一併指定。\
例如：

```python
onnx_output = torch.onnx.export(model2, (dummy_input, hidden), model_onnx_path, verbose=False)
```

此處隱藏層並非僅指RNN中的隱藏層，不同的模型嫁接也會被視為是隱藏層。



## Keras to TensorFlow Lite

Tensorflow 提供 converter，可直接對 keras 模型進行轉換。

Tensorflow 1.X 版本：
Converter 轉換 keras 模型的 input 為 `HDF5` 檔案，因此需要事先將完整模型儲存，不可只存 weights。\
若有自定義 `function` 需設定 `custom_objects`。

```python
tensorflow.lite.TFLiteConverter.from_keras_model_file(path).convert()
tensorflow.lite.TFLiteConverter.from_keras_model_file(path, custom_objects={'function':function}).convert()
```

Tensorflow 2.0 版本：\
其 input 的 `model` 為 `tf.Keras.Model`。

```python
tensorflow.lite.TFLiteConverter.from_keras_model(model).convert()
```

<!--## Keras Houeglass Network to TensorFlow Lite-->
## Yolo3 to TensorFlow Lite

桌機版程式碼使用 [keras-yolo3](https://github.com/qqwweee/keras-yolo3) 做轉換實驗。\
可直接使用 TensorFlow Lite converter 進行轉換。

#### Android 端：
確認 `app.build.gradle` 中加入以下設定：

```java
android {
    ...
    aaptOptions {
        noCompress "tflite"
    }
}

dependencies {
    ...
    implementation 'org.tensorflow:tensorflow-lite:0.0.0-nightly'
}
```
詳細可參考 [Android quickstart](https://www.tensorflow.org/lite/guide/android)、[Android-TensorFlow-Lite-Example](https://github.com/amitshekhariitbhu/Android-TensorFlow-Lite-Example/blob/master/app/build.gradle)。



#### tflite predict：

Predict 使用 `run(input, output)` 方法。\
所需的 `input` 格式為 `ByteBuffer`。\
需自行將 Android image type: `Bitmap` 轉為`ByteBuffer`。\
`output` 格式為 `float array`，需要事先宣告 output size。

Tflite converter 不支援 yolo head，因此需要自行將 predict 結果轉換成 bbox 及類別標籤。\
原 keras-yolo3 的模型 output 為多尺度輸出，但轉換成 lite 後僅剩第一個尺度 `[bs][13][13][255]` 的輸出。\
因此轉換為 tensorFlow lite 後，能夠偵測出的物件大小僅限定在一個較小的範圍內。
