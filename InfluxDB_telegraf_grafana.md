# InfluxDB + telegraf + grafana 筆記
[教學](https://ssorc.tw/7306/%E4%BD%BF%E7%94%A8-influxdb-telegraf-grafana-%E6%94%B6%E9%9B%86%E4%B8%BB%E6%A9%9F%E8%B3%87%E6%96%99%E3%80%81%E7%9B%A3%E6%8E%A7%EF%BC%8C%E4%B8%A6%E5%9C%96%E5%BD%A2%E5%8C%96%E5%88%86%E6%9E%90/)

## 目錄
* [InfluxDB](#influxdb)
  * [安裝並啟動](#安裝並啟動)
  * [設定檔修改](#設定檔修改)
  * [進入資料庫環境](#進入資料庫環境)
  * [建立使用 https 需要的憑證](#建立使用-https-需要的憑證)
  * [啟用身份檢查並改用 https 連線](#啟用身份檢查並改用-https-連線)
  * [身份檢查 + https 連線](#身份檢查-https-連線)
  * [指令](#指令)

* [Telegraf](#telegraf)

* [Grafana](#grafana)

---

## InfluxDB
[官方安裝教學](https://docs.influxdata.com/influxdb/v1.7/introduction/installation/)<br>
[InfluxDB API reference](https://docs.influxdata.com/influxdb/v1.7/tools/api/)<br>
[使用 InfluxDB API write data](https://docs.influxdata.com/influxdb/v1.7/guides/writing_data/)

### 安裝並啟動
```
wget -qO- https://repos.influxdata.com/influxdb.key | sudo apt-key add -
source /etc/lsb-release
echo "deb https://repos.influxdata.com/${DISTRIB_ID,,} ${DISTRIB_CODENAME} stable" | sudo tee /etc/apt/sources.list.d/influxdb.list
sudo apt update
sudo apt install influxdb
sudo systemctl unmask influxdb.service
sudo systemctl start influxdb
```

---

### 設定檔修改
```
sudo vim /etc/influxdb/influxdb.conf 
```

#### 資料儲存
資料儲存的地方：
```
[meta]
  dir = "/var/lib/influxdb/meta"

[data]
  dir = "/var/lib/influxdb/data"
  wal-dir = "/var/lib/influxdb/wal"
```

如果改了儲存位置，要給權限
```
chown influxdb:influxdb your_dir
```
還要給 init.sh 的權限<br>
在`influxdb`資料夾中 run script:
```
if [ ! -f "$STDOUT" ]; then
    mkdir -p $(dirname $STDOUT)
    chown $USER:$GROUP $(dirname $STDOUT)
 fi

 if [ ! -f "$STDERR" ]; then
    mkdir -p $(dirname $STDERR)
    chown $USER:$GROUP $(dirname $STDERR)
 fi

 # Override init script variables with DEFAULT values
```

#### 啟用 http
```
[http]
  enabled = true
  bind-address = ":8086"
  auth-enabled = false
```

設定更改完成，重新啟動
```
sudo systemctl restart influxdb
```

檢查狀態<br>
按`q`離開
```
sudo systemctl status influxdb
```

---

### 進入資料庫環境
```
influx
```

建立最高權限管理者
```
CREATE USER admin WITH PASSWORD admin WITH ALL PRIVILEGES
```

建立資料庫
```
create database mydb
```

檢查
```
show databases
show users
```

---

### 建立使用 https 需要的憑證
`-days`：憑證有效天數
```
sudo openssl req -x509 -nodes -newkey rsa:2048 -keyout /etc/ssl/influxdb-selfsigned.key -out /etc/ssl/influxdb-selfsigned.crt -days 30
```

更改權限
```
sudo chmod 644 /etc/ssl/influxdb-selfsigned.crt
sudo chmod 600 /etc/ssl/influxdb-selfsigned.key
```

放憑證的資料夾要給訪問權限（可將憑證檔移到自己想放的地方）
```
chown influxdb:influxdb your_ca_dir
```

---

### 啟用身份檢查並改用 https 連線
[官方教學](https://docs.influxdata.com/influxdb/v1.7/administration/https_setup/)

```
sudo vim /etc/influxdb/influxdb.conf 
```

身份檢查
```
[http]
  auth-enabled = true
```

啟用 https
```
[http]
  https-enabled = true
  https-certificate = "/your/ca/path/influxdb-selfsigned.crt"
  https-private-key = "/your/ca/path/influxdb-selfsigned.key"
```

設定更改完成，重新啟動
```
sudo systemctl restart influxdb
```

---

### 身份檢查 + https 連線
因為使用自簽的憑證，要加`-unsafeSsl`<br>
`-host`、`-port`，若為預設值可不給
```
influx -ssl -unsafeSsl -host localhost -port 8086 -username 'admin' -password 'admin'
```

#### 用 curl 連線
查詢總共有哪些資料庫<br>
因為使用自簽的憑證，略過檢查：`-k`
```
curl -k https://localhost:8086/query -u admin:admin --data-urlencode "q=SHOW DATABASES"
```

將資料寫入`mydb`資料庫<br>
格式："`key` `field` `time`"<br>
`key`：包含 measurement name、tag key 及 tag value
* measurement name： sql 的 table name
* tag key 及 tag value：可多組，以逗號隔開

`field`：此 table 要儲存的資料，包含 field key 及 field value<br>
`time`： 可自設，如未設定，系統會自動填入時間<br>

```
curl -k -X POST "https://localhost:8086/write?db=mydb" -u admin:admin --data-binary "[measurement name],[tag key]=[tag value],[tag key]=[tag value] [field key]=[field value],[field key]=[field value] [時間戳記(可選)]"
```

---

### 指令

建立名叫`mydb`的資料庫
```
create database mydb
```

建立最高權限使用者
```
CREATE USER admin WITH PASSWORD 'admin' WITH ALL PRIVILEGES
```
看剛剛是否成功建立
```
show databases
show users
```

讓使用者`telegraf`得到資料庫`telegraf`的權限
```
grant all on telegraf to telegraf
```
刪除資料庫
```
DROP DATABASE mydb
```
查詢資料庫裡面某個 measurement 裡的全部資料
```
use mydb
show measurements
select * from measurement_name
```


---

## Telegraf

安裝
```
sudo apt install -y telegraf
systemctl enable telegraf
```
測試可否取得資訊
```
telegraf -test -config /etc/telegraf/telegraf.conf --input-filter cpu
```
更改設定檔
```
sudo vim /etc/telegraf/telegraf.conf
```
重新啟動
```
sudo systemctl restart telegraf
```
---

## Grafana
[官網安裝教學](https://grafana.com/docs/grafana/latest/installation/debian/)

安裝
```
wget -q -O - https://packages.grafana.com/gpg.key | sudo apt-key add -
sudo add-apt-repository "deb https://packages.grafana.com/oss/deb stable main"
sudo apt-get update
sudo apt-get install grafana
```

設定及啟動
```
sudo systemctl daemon-reload
sudo systemctl start grafana-server
sudo systemctl enable grafana-server
```

登入網址
```
localhost:3000
```
