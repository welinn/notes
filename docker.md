# Docker 相關

* [環境安裝](#環境安裝)
* [使用GPU所需套件](#使用gpu所需套件)
* [指令](#指令)
* [docker run 參數](#docker-run-參數)
* [坑](#坑)
* [dockerfile](#dockerfile)
* [rpi](#rpi)



### 環境安裝

[安裝教學](https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository)<br>

1. 更新並安裝 packages 讓 apt 可以透過 https 使用 repository

```
sudo apt-get update

sudo apt-get install \
     apt-transport-https \
     ca-certificates \
     curl \
     gnupg \
     lsb-release

```

2. 新增 docker 的 GPG key

```
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
```

3. 設定 stable repository

```
 echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

4. 安裝 docker

```
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
```

**讓 docker 不用 sudo 就能跑：**
```bash
sudo usermod -aG docker [username]
```

上面沒用的話：
```bash
sudo groupadd docker
sudo gpasswd -a ${USER} docker
sudo chmod a+rw /var/run/docker.sock
```

### 使用GPU所需套件

使用 tensorRT 官方教學進入環境<br>
`--gpu` 使用 gpu<br>
`--rm` 每次執行完就把 container 刪掉<br>
`nvcr.io/nvidia/tensorrt:21.06-py3` 想要使用的 docker image<br>
```
sudo docker run --gpus all -it --rm -v /path/for/mount/folder:/docker_mount nvcr.io/nvidia/tensorrt:21.06-py3
```

出現錯誤:
```
Error response from daemon: could not select device driver "" with capabilities: [[gpu]].
```

要安裝 `nvidia-container-toolkit`<br>
需要從 [官方安裝教學](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html) 取得 key<br>


加 key：(用官方網站的)
```
distribution=$(. /etc/os-release;echo $ID$VERSION_ID) \
&& curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey ︱ sudo apt-key add - \
&& curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list ︱ sudo tee /etc/apt/sources.list.d/nvidia-docker.list
```

安裝：
```
sudo apt update
sudo apt install nvidia-container-toolkit
```

裝完要重啟：
```
sudo systemctl restart docker
```


### 指令

`docker ps -a` : 看現在有哪些 container<br>
`docker logs -n 5 container_id` : 看 log，`-n` 指定只顯示尾巴幾行<br>
`docker rm container_id` : 刪掉 container<br>
`docker images` : 看有哪些 image<br>
`docker rmi image_name` : 刪掉 image<br>
`docker commit container_id REPOSITORY:TAG` : 把 container_id 的 container 做成 image，之後叫起來的時候 image 名稱就打 REPOSITORY:TAG<br>
`docker load -i XXX.tar` : 把從其他地方用 `docker save` 存出來的 image tar 檔匯入
* 匯入匯出 image：<br>
  * `docker export` 要用 import 匯入
    * export：匯出 image 跟 containers
  * `docker save` 要用 load 匯入
    * save：匯出 image

`ctrl-p` + `ctrl-q` : 不關閉 container 離開<br>

##### 回到 container 環境

`docker exec`、`docker attach` 差別：<br>
docker 預設 pid=1 的執行緒死掉，container 就關掉<br>
一開始 docker run 時尾巴有下指令(例如 bash)，該指令會被設成 pid=1<br>

例如指令：<br>
`docker exec -it AAA bash`<br>
尾巴 bash 會被設成 pid=1<br>


假設一開始指令是跑一個 python 程式，container 起來就會跑它<br>
使用 ctrl-p + ctrl-q 離開 container 後，如果要回去看檔案之類的<br>
這時用 `docker attach CONTAINER_NAME` 會跑到正在跑的那個程式那邊，把程式停掉 container 就被關了，東西也看不到<br>
用 `docker exec -it CONTAINER_NAME bash` 回去，就會開一個新的執行緒進去，不會動到 pid=1 的那個<br>


### docker run 參數

* `--ipc host -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix` : 開圖形介面<br>
  其中 `--ipc host` 未實測是否為必要參數
* `--network host` : 讓 container 可以連到本機 localhost<br>
* `--shm-size=16G` : 可設定 `/dev/shm` 的 size (用 `df -lh` 可看到 size)<br>
若遇到以下 error，將 `/dev/shm` size 加大可解決
```
/opt/conda/lib/python3.8/multiprocessing/resource_tracker.py:216: UserWarning: resource_tracker: There appear to be 1 leaked shared_memory objects to clean up at shutdown
  warnings.warn('resource_tracker: There appear to be %d '
Bus error (core dumped)
```

### 坑
* docker pull 出現網路 408：[github 討論](https://github.com/moby/moby/issues/22635)<br>
改設定：
```bash
sudo sysctl -w net.ipv4.tcp_mtu_probing=1
```

* 執行程式時遇到 error：
```
No protocol specified
Unable to init server: Could not connect: Connection refused

(Result:334): Gtk-WARNING **: 07:36:59.958: cannot open display: :1
-------------------------------------------------------------------
PyCUDA ERROR: The context stack was not empty upon module cleanup.
```
這個狀況代表 X window 開不起來，先關掉 container，回到本機下指令，給 docker 權限去開視窗
```bash
xhost local:docker
xhost +local:docker
```
再用 docker run 重新開 container 就可以了


### dockerfile

改時區：
```dockerfile
RUN apt-get update
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends tzdata
RUN TZ=Asia/Taipei \
    && ln -snf /usr/share/zoneinfo/$TZ /etc/localtime \
    && echo $TZ > /etc/timezone \
    && dpkg-reconfigure -f noninteractive tzdata 
```

---

## rpi

##### 樹莓派裝 docker
```bash
sudo apt-get update && sudo apt-get upgrade
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
```

##### 用 Dockerfile 在樹莓派上編opencv：

1. image 要拉 `arm32v7` 的來源（硬體不一樣）
  用 python:3.8 python:3.9 會壞<br>
  好像是因為不支援 arm ([討論](https://stackoverflow.com/questions/60566831/installing-opencv-for-docker-on-raspberry-pi3))
  ```
  FROM arm32v7/python:3.7-buster
  ```

2. 刪掉 `-D WITH_IPP=ON`

   這個好像是 windows only

3. `make -j$(nproc)` 改 make 就好

   因為樹莓派記憶體會不夠，不能用 `make -j4` 這種跑

##### 疑難雜症

跑程式出現：
```
OSError: Could not find library geos_c or load any of its variants ['libgeos_c.so.1', 'libgeos_c.so']
```

安裝函式庫：
```bash
apt-get update
apt-get install libgeos++
```

