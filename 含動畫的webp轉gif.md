## 含動畫的webp轉gif

套件：[webp](https://pypi.org/project/webp/), [github](https://github.com/anibali/pywebp)

* 先把 webp 轉 png
```python
import webp, cv2

imgs = []
with open('input.webp', 'rb') as f:
    webp_data = webp.WebPData.from_buffer(f.read())
    dec = webp.WebPAnimDecoder.new(webp_data)
    for arr, timestamp_ms in dec.frames():
        #arr: nparray
        #timestamp_ms: 毫秒字串
        #print(arr.shape)
        imgs.append(arr)

for i, im in enumerate(imgs):
    cv2.imwrite(f'{i}.png' im)
```

* 用 convert 轉 gif
  * `-delay`： 控制 fps

```bash
convert -delay 20 *.png output.gif
```

