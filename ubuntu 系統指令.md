* [雜七雜八](#雜七雜八)
* [桌面環境](#桌面環境)
* [bashrc](#bashrc)
* [行程](#行程)
* [find](#find)
* [硬碟、容量](#硬碟容量)
  * [開機自動掛載](#開機自動掛載)
* [檔案內容操作](#檔案內容操作)

### 雜七雜八

| | |
| ---  | --- |
|sudo apt upgrade                     |更新可安裝資訊|
|sudo apt update                      |安裝更新|
|sudo apt autoremove                  |刪不要的東西|
|sudo deluser --remove-home [username]|刪 ubuntu user|
|apt list \| grep libssl              |查 apt 裝的 libssl 的詳細資訊|
|find -name *.yuv                     |搜尋所有的 yuv 檔|
|lspci \| grep -Pi 'ethernet\|network'|看網卡型號|
|watch -n 10 nvidia-smi               |每10秒顯示一次GPU使用狀況|
|chown user:user                      |改檔案的 擁有者：群組|
|gnome-system-monitor                 |系統監視器，可看 cpu mem network 使用狀況（GUI）|
|gnome-control-center                 |開系統設定值（GUI）|
|sudo lsof -i -P -n \| grep LISTEN    |查使用中的 port|
|vim /var/log/syslog                  |看系統 log|
|iconv -f BIG-5 -t UTF-8 a.txt > b.txt|改編碼|
|sudo dpkg-reconfigure tzdata         |改時區|
|date                                 |印出現在系統時間|
|type -a python3                      |找 python3 這個指令安裝的完整路徑在哪|
|ll /usr/bin/python3                  |看系統的 python 別名被指定成什麼|

* 撈 dpkg licenses
```bash
packages=`dpkg --get-selections | awk '{ print $1 }'`
for package in $packages; do echo "$package: "; cat /usr/share/doc/$package/copyright; echo ""; echo ""; done > ~/licenses.txt
```


### 桌面環境

* 遇到 `Warning: Ignoring XDG_SESSION_TYPE=wayland on Gnome. Use QT_QPA_PLATFORM=wayland to run on Wayland anyway.`
* 檢查用哪種桌面環境
```bash
echo $XDG_SESSION_TYPE
```
* 從 wayland 改回 x11
  1. `sudo vim /etc/gdm3/custom.conf` 把 `WaylandEnable=false` 打開
  2. `sudo vim /etc/environment` 加上 `QT_QPA_PLATFORM=xcb`
  3. reboot

### bashrc

指定別名
```bash
alias python3="/usr/bin/python3"
```


### 行程
| | |
| ---  | --- |
|top                                  |可看PID|
|kill -9 PID                          |強制刪除PID的程式|
|kill PID                             |刪除PID的程式|
|pkill 應用程式名稱(Ex. firefox)        |刪除應用程式|
|ps -ef \| grep [想查的東西]            |查執行中的程序，ex: ps -ef \| grep python|
|cat /proc/sys/fs/file-max            |看系統可開啟最大檔案數   |
|ulimit -n                            |看 user 可開啟最大檔案數 |
|ulimit -n 200000                     |暫時性更改可開檔案數上限  |
|sudo lsof -p 123                     |看行程 PID=123 目前開了哪些檔案|
|sudo lsof -p 123 \| wc -l            |只顯示開了幾個檔案（算上一個指令會顯示幾行）|
|grep files /proc/$(pidof influx)/limits |看單一行程 (範例：influx) 的可開檔案數上限(軟體/硬體)<br>可先用 top 看 PID，proc 資料夾各行程檔案中以ID資料夾表示|
|prlimit --pid 123                    |看 PID=123 的 proc/123/limits 限制內容|
|prlimit --pid 123 --nofile=1024:4096 |改 PID=123 的可開檔案數（軟體:硬體）<br>選項 --nofile：用 prlimit 查出的 RESOURCE 名稱改小寫|



### find
* `-type`
  * `f`：檔案
  * `d`：資料夾

* 計算檔案個數：
  * `find`：在 `.` 中找到所有檔案（會 walk 到子資料夾）
  * `wc`：算有幾行
  ```bash
  find . -type f | wc -l
  ```
* 計算資料夾個數：
  ```bash
  find . -type d | wc -l
  ```


### 硬碟、容量

* 看硬碟使用量
```bash
df -h
```
* 看硬碟使用量 (不顯示 loop)
```bash
df -h | grep -v ^/dev/loop
```
* 看資料夾佔用多少硬碟空間
```bash
du -sh [folder_name]
```
* 看當前位置所有資料夾各佔多少硬碟空間並排序 
```bash
sudo du -chd 1 | sort -h
```
* 看資料夾容量
```bash
du --max-depth=1 -B M | sort -g
```
* 檢視所有分割槽&硬碟是哪個系統
```bash
sudo fdisk -l
```
* 檢視所有分割槽 (不顯示 loop) 
```bash
sudo fdisk -l | sed -e '/Disk \/dev\/loop/,+5d'
```
##### 開機自動掛載
* 看硬碟大小、位置、分割狀態
```bash
lsblk
```
* 看硬碟ID
  * 顯示全部
  ```bash
  sudo blkid
  ```
  * 只印有 UUID 的
  ```bash
  sudo blkid -s UUID
  ```
**如果硬碟有分割，但沒有自己的 UUID，要先刪除分割區，再格式化**
* 刪除分割區
  1. 確認硬碟名稱，使用 fdisk，如：`fdisk /dev/sda`
  2. 按 `d` 刪除分割
  3. 按 `w` 儲存變更
* 格式化
```bash
sudo mkfs.ext4 /dev/sda
```
* 掛載
  * 開啟檔案 `sudo vim /etc/fstab` 新增硬碟
  ```bash
  UUID=XXXXXXXX /mount_point/    ext4    defaults    0    1
  ```
  * 記得要建立掛載位置的資料夾
  * `sudo mount -a` 將剛剛寫入 fstab 的硬碟掛上去

### 檔案內容操作
* 顯示檔案有幾行
```bash
wc -l file.log
```
* 每指定的行數切一個檔案
```bash
split -l 60000 file.log
```
* 印出檔案內容
  * 印出檔案中第5行
  ```bash
  sed -n 5p file.log
  ```
  * 印出檔案中 5 & 8 行
  ```bash
  sed -n -e 5p -e 8p file.log
  ```
  * 印出檔案中 5 ~ 8 行
  ```bash
  sed -n 5,8p file.log
  ```
  * 印出檔案中 5 ~ 8 & 10 行
  ```bash
  sed -n -e 5,8p -e 10p file.log
  ```


