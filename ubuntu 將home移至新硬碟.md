# 將home移至新硬碟

| | |
| ---  | --- |
|sudo mkdir /mnt/home             ||
|sudo blkid                       |查新硬碟UUID|
|sudo vim /etc/fstab              |將新硬碟的mount設定寫入，mount到/mnt/home|
|sudo mount /dev/xxxx             |把新硬碟掛載|
|sudo rsync -av /home/ /mnt/home/ |把原本home的資料複製到新硬碟（包含擁有者等資訊|
|sudo mv /home/ /home_backup/     |原本home更名|
|sudo mkdir /home                 |建立home資料夾|
|sudo umount /dev/xxxx            |新硬碟卸載|
|sudo blkid                       |改掛載資訊，新硬碟掛到 /home 底下|
|sudo mount /dev/sdbx             |掛載新硬碟|

確認成功就可以刪除 home_backup
