* [graph 相關](#graph-相關)
* [diff](#diff)

---

**初始設定**
```
git config --global user.email “xxx@gmail.com”
git config --global user.name “xxx”
```

**走 http 儲存帳密**
* 單一專案
```
git config credential.helper store
```
* 全域
```
git config --global credential.helper store
```

**顯示全域設定**
```
git config --global --list
git config --show-origin --list
```

**取消全域設定**
```
git config --global --unset [要取消的項目]
git config --global --unset user.name
```

**關掉 SSL 驗證**
```bash
git config --global http.sslVerify false
```

**直接從本地端上傳新專案，不透過網頁建立**
```bash
git push --set-upstream https://gitlab.com/[username]/[project_name].git master
```

**拉資料**
`git pull` = `git fetch` + `git merge`
```
git pull origin [branch name]
```

**改 buffer**
```bash
git config --global http.postBuffer 157286400
```

**看連到遠端的網址**
```
git remote -v
```

**移除遠端**
```
git remote rm origin
```

**要上傳的檔案超過大小限制**
```
git filter-branch --tree-filter 'rm -rf path/to/file' HEAD
git push
```

**更新本機 git 版本**
```bash
sudo add-apt-repository ppa:git-core/ppa
sudo apt update
sudo apt install git
```

### graph 相關

**commit 寫錯重設**
```
git reset master^
git reset [commit 編號]^
```

**放棄 local 端所有更改**
```
git reset --hard origin/[branch name]
```

**放棄 local 端單一檔案更改**
```
git checkout origin/[branch name] -- [file name]
```

**刪除已 push 的節點**
* 退到前一個之後強制 push
```bash
git reset master^
git push -f
```

**想把A分支的內容合併進B分支，但不想動到B分支裡的某些檔案**

* checkout 到 B 分支
```bash
git checkout branch_B
```

* 把 A 併進 B，但不要 commit（效果等同 git add）
```bash
git merge --no-ff --no-commit branch_A
```

* 取消更改不想動到的檔案
```bash
git reset path/for/file1 path/for/file2 path/for/dir
```

* 把不想動到的檔案 checkout 回 HEAD 的檔案（現在 HEAD 指在 branch_B）
```bash
git checkout HEAD path/for/file1 path/for/file2 path/for/dir
```


### diff

##### 比較不同 repo

先把想比較的遠端網址加到現在這個專案，`-f` 應該是 fetch 的意思<br>
可自己指定加入的遠端要取什麼名稱<br>
可以指定要無視哪些檔案<br>
比較完之後，把剛加進來的遠端刪掉就好

* `--name-only`：只顯示不一樣的檔名
* `':!*.xml'`：不比較 `*.xml`

```bash
git remote add -f b XXX.git
git remote update
git diff --name-only master b/master ':!*.xml' ':!*.yaml' ':!*.json' ':!folder/sub_folder'
git remote rm b
```

##### 只比較特定檔案

```bash
git diff master b/master -- folder/filename.py
```

有換行 `\n` 跟 `\r\n` 的問題：（有一堆 `^M`）<br>
加 `-b` 忽略空白
```bash
git diff -b master b/master -- folder/filename.py
```

