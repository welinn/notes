# 個人化設定

### 垃圾桶加到左邊Dock

寫一個 bash 檔：
```
vim trashcan.sh
```

內容：
```bash
#!/bin/bash
icon=$HOME/.local/share/applications/trashcan.desktop

while getopts "red" opt; do
 case $opt in
 r)
  if [ "$(gio list trash://)" ]; then
   echo -e '[Desktop Entry]\nType=Application\nName=Trash\nComment=Trash\nIcon=user-trash-full\nExec=nautilus trash://\nCategories=Utility;\nActions=trash;\n\n[Desktop Action trash]\nName=Empty Trash\nExec='$HOME/Documents/trashcan.sh -e'\n' > $icon
  fi
  ;;
 e)
  gio trash --empty && echo -e '[Desktop Entry]\nType=Application\nName=Trash\nComment=Trash\nIcon=user-trash\nExec=nautilus trash://\nCategories=Utility;\nActions=trash;\n\n[Desktop Action trash]\nName=Empty Trash\nExec='$HOME/Documents/trashcan.sh -e'\n' > $icon
  ;;
 d)
  while sleep 5; do ($HOME/Documents/trashcan.sh -r &) ; done
  ;;
 esac
done
```

改權限：
```
chmod +x trashcan.sh
```

執行：
```
./trashcan.sh -e
```

現在可在 application 中找到 Trash 了，右鍵加到最愛

### 將 show application 移到頂端

```
gsettings set org.gnome.shell.extensions.dash-to-dock show-apps-at-top true
```

### 軟體中心：GNOME Tweaks

##### 調叉叉位置

`windows` -> `Titlebar Buttons` -> `Placement` -> `left`

##### 桌面垃圾桶消除

`Desktop` -> `Trash`


