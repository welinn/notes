# vnc 筆記
使用 TightVNC
* [Server 端安裝及設定](#server-端安裝及設定)
* [Client 端滑鼠黑叉叉 & 畫面顯示灰屏或網格狀](#client-端滑鼠黑叉叉-畫面顯示灰屏或網格狀)

註：速度慢，可改用 x2go 進行遠端連線

### Server 端安裝及設定
* 更新
```
sudo apt update
```
* 下載 Xfce 桌面
```
sudo apt-get install xfce4 xfce4-session
```
* 下載 TightVNC server
```
sudo apt-get install tightvncserver
```
* 啟動 TightVNC server
```
tightvncserver
```
* 登入密碼設定
  * 第一次啟動會要求設定密碼，長度最長限制 = 8<br>過長會被截掉
```
You will require a password to access your desktops.

Password:
Verify:
```
* 是否設成僅能觀看密碼
  * 若設成`view-only`，登入後僅能觀看視窗畫面<br>設定完成後會顯示開了哪個 port
```
Would you like to enter a view-only password (y/n)?
```


* 刪除　5902　port　的 server
```
tightvncserver -kill :2
```
* 開 5903 port 的 server
```
tightvncserver :3 -geometry 1080x768 -depth 24
```
or
```
vncserver :3 -geometry 1080x768 -depth 24
```

* 看開了多少 server
```
ps -ef | grep vnc
sudo netstat -tnlp | grep vnc
```
* 顯示開著的 server 的 id
```
cat ~/.vnc/*.pid
```

### Client 端滑鼠黑叉叉 & 畫面顯示灰屏或網格狀

更改設定檔 `~/.vnc/xstartup`

```sh
#!/bin/bash
export XKL_XMODMAP_DISABLE=1
unset SESSION_MANAGER
unset DBUS_SESSION_BUS_ADDRESS
exec /etc/X11/xinit/xinitrc
[ -x /etc/vnc/xstartup ] && exec /etc/vnc/xstartup
[ -r $HOME/.Xresources ] && xrdb $HOME/.Xresources
xsetroot -solid grey
vncconfig -iconic &
#xterm -geometry 80x24+10+10 -ls -title "$VNCDESKTOP Desktop" &
#twm &
gnome-session &
```

### Client 端連線
下載 vncviewer
```
sudo apt install vncviewer
```

