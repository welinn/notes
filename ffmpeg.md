[14.04安裝ffmpeg](https://stackoverflow.com/questions/29125229/how-to-reinstall-ffmpeg-clean-on-ubuntu-14-04)
```bash
sudo apt-get --purge remove ffmpeg
sudo apt-get --purge autoremove

sudo apt-get install ppa-purge
sudo ppa-purge ppa:jon-severinsson/ffmpeg

sudo add-apt-repository ppa:mc3man/trusty-media
sudo apt-get update
sudo apt-get dist-upgrade

sudo apt-get install ffmpeg
```

help 中常用參數：
```bash
ffmpeg -h
```
`-i`  : input<br>
`-vn` : disable video<br>
`-an` : disable audio<br>
`-b:a`: set audio bitrate<br>
`-b:v`: set video bitrate<br>

---

* 截短影片：(時間設定：`hh:mm:ss`)
  ```bash
  ffmpeg -i 原始影片 -ss 開始時間 -t 擷取長度 -vcodec copy -acodec copy 擷取後片斷檔名
  ```

* 合併影片
  ```bash
  ffmpeg -i "concat:input_1.avi|input_2.avi" -c copy output.avi
  ```


* mp4 轉 mp3
  ```bash
  ffmpeg -i input.mp4 -b:a 192K -vn output.mp3
  ```
  寫成 shell：
  ```bash
  for f in *.mp4; do ffmpeg -i "$f" -b:a 192K -vn "${f%.mp4}".mp3; done
  ```

* mp4 轉 jpg
  ```bash
  ffmpeg -i in.mp4 -r 1 out/output-%05d.jpg
  ffmpeg -i in.mp4 -r 1 -t 40 out/output-%05d.jpg
  ```
  `-r`: fps<br>
  `-t`: 要取出的時間(sec) => -r 1 -t 40 = 取40張<br>


* avi 轉 mp4
  ```bash
  ffmpeg -i 片源.avi -codec:v libx264 -codec:a libfaac out.mp4
  ffmpeg -i source.avi out.mp4
  ```

* 影片轉 gif
  * `scale`：輸出的 gif 大小
  ```bash
  ffmpeg -ss 4 -t 7 -i input.avi -vf "fps=10,scale=720:-1:flags=lanczos,split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse" output.gif

  ```
  
* 轉成手機可播放格式 [來源](https://askubuntu.com/questions/734922/videos-which-are-converted-in-linux-do-not-play-in-mobile-browser)
  * `-profile:v baseline -level 3.0`：最大程度相容各種設備
  * `-pix_fmt yuv420p`：允許在更廣泛的設備上播放
  * `-movflags +faststart`：可在完全下載之前就先線上播放
  ```bash
  ffmpeg -i input.mp4 -c:v libx264 -preset slow -crf 22 \
       -profile:v baseline -level 3.0 \
       -movflags +faststart -pix_fmt yuv420p \
       -c:a aac -b:a 128k \
       output.mp4
  ```

* 影片畫面裁剪
  ```bash
  ffmpeg -i in.mp4 -filter:v "crop=out_w:out_h:x:y" out.mp4
  ```
  `out_w`：輸出影片寬<br>
  `out_h`：輸出影片高<br>
  `x`, `y`：左上角座標<br>


* rtsp 串流存檔 (存一個 60 秒的檔案)
  ```bash
  ffmpeg -i [rtsp 網址] -t 60 /path/for/video/"$(date +"%Y_%m_%d_%I_%M_%p")".mp4
  ffmpeg -i [rtsp 網址] -t 60 /path/for/video/"$(date +"%Y_%m_%d_%H_%M_%S")".mp4
  ffmpeg -i [rtsp 網址] -t 60 /path/for/video/"$(date +"%Y_%m_%d_%X")".mp4
  ```


* 螢幕錄影
  ```bash
  ffmpeg -video_size 1920x1080 -framerate 30 -f x11grab -i :0.0+1366,0 -f pulse -ac 2 -i 1 -c:v libx264rgb -crf 0 -preset ultrafast sample.mkv
  ```
  * 速率控制 (Rate control)

    `-crf`：<br>
    h264 設定值範圍 0 - 51<br>
    0 為最高品質，預設值為 23，合理範圍 18 - 28<br>
    CRF 18 近視覺無損
          
  * 畫面參數 `-i :0.0+1366,0`
    ```bash
    echo $DISPLAY
    ```
    將得到的結果填入 `-i` 參數，如：`-i :0`<br>
    若要指定錄影的範圍，則在這邊的 `-i` 參數後面用 `+` 指定**左上角**位置，如：`-i :0.0+1366,0`<br>
    錄影的寬高指定在 `-video_size` 參數<br>

  * 聲音參數 `-i 1`
    ```bash
    pacmd list-sources
    ```
    從查詢結果中選擇要錄的音源的 index<br>
    選擇的音源可能會在 properties 裡面 device.description 顯示如 Monitor of Built-in Audio Analog 之類的內容<br>


* 影片消除聲音
  ```bash
  ffmpeg -i input.mp4 -c copy -an output.mp4
  ```
  
* 調整分貝 (原音量 +/- 多少分貝)
  ```bash
  ffmpeg -i in.m4v -vcodec copy -filter:a "volume=30dB" out.mp4
  ffmpeg -i in.m4v -vcodec copy -filter:a "volume=-30dB" out.mp4
  ```

* 錄音
  ```bash
  ffmpeg -f pulse -i default output.wav
  ```
* 抽換聲音
  * 將 mp4 聲音用其他音檔取代
  * `-shortest`：當音檔比影片長時，縮短它
  * `-c:v copy`：影像直接複製，比較快，但重新編碼音檔，避免相容性跟同步問題
  * `-map 0:v:0`：將 index 0(輸入的第0個來源) 的視訊(v) map 到 index 0
  * `-map 1:a:0`：將 index 1(輸入的第1個來源) 的音訊(a) map 到 index 0
  ```bash
  ffmpeg -i in.mp4 -i in.wav -c:v copy -map 0:v:0 -map 1:a:0 -shortest new.mp4
  ```
* 抽換影格
  * 範例：把 a.mp4 裡面的影格放進 b.mp4
  * 這邊指令預設為固定fps，frame rate 不固定的更複雜

  1. 把指定張數的 frame 存成圖

  範例：存 frame 第 416 到 489，共74張
  ```bash
  ffmpeg -i a.mp4 -vf "select='between(n\,416\,489)'" -vsync passthrough '%6d.png'
  ```
  2. 看給定的時間是第幾張 frame，找到想替換的 frame 的時間在哪裡
  ```bash
  ffmpeg -t 00:13.83 -i b.mp4 -nostats -vcodec copy -y -f rawvideo /dev/null  2>&1  | grep frame | awk '{print $2}'
  ```
  3. 指令 `ffmpeg -i b.mp4` 如果是固定 fps，就可以看到 frame rate
  4. 把剛剛從 a.mp4 撈出來的圖片塞進 b.mp4，`-itsoffset` 是要塞 frame 的起始秒數
  ```bash
  ffmpeg -i b.mp4 -itsoffset 13.83 -framerate 30 -i '%06d.png' -filter_complex "[0:v:0][1]overlay=eof_action=pass" output.mp4
  ```

* 調整 FPS
  * 指令中2個fps：第一個 fps 是過濾器的名稱，第二個 fps 是輸入參數
  ```bash
  ffmpeg -i input.mp4 -filter:v fps=fps=30 output.mp4
  ```
  * 只寫一個fps似乎也可
  ```bash
  ffmpeg -i input.mp4 -filter:v fps=30 output.mp4
  ```
---

### Error
```
[libx264 @ 0x55913a1375c0] width not divisible by 2 (565x1003)
Error initializing output stream 0:0 -- Error while opening encoder for output stream #0:0 - maybe incorrect parameters such as bit_rate, rate, width or height
```
error 原因：input 的 size 無法被2整除<br>
解法：在轉檔時加上參數補邊（預設補黑色）：`-vf "pad=ceil(iw/2)*2:ceil(ih/2)*2"`<br>
若要補其他顏色（例如改白色）則在後面加上： `:color=white`，詳情：[pad 文件](https://ffmpeg.org/ffmpeg-filters.html#pad)



