# Android 筆記
* [Java code](#java-code)
* [Setting](#setting)
* [打包 jar / aar](#打包-jar-aar)
* [匯入](#匯入)
* [Android studio VM](#android-studio-vm)
* [網路連線](#網路連線)
* [改名](#改名)
* [存取設定資料](#存取設定資料)


## Java code
* [螢幕隨時亮著](#螢幕隨時亮著)
* [螢幕不隨手機轉 (固定螢幕方向)](#螢幕不隨手機轉-固定螢幕方向)
* [檢查是否設定固定螢幕方向](#檢查是否設定固定螢幕方向)
* [檢查系統螢幕旋轉](#檢查系統螢幕旋轉)
* [將檔案寫在非 root 無法拜訪的地方](#將檔案寫在非-root-無法拜訪的地方)

### 螢幕隨時亮著
```java
getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
```
### 螢幕不隨手機轉 (固定螢幕方向)
```java
setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
```
### 檢查是否設定固定螢幕方向
```java
//in MainActivity
if (getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_NOSENSOR){
    //Do something
}

//in Other Activity
//activity 變數為希望檢查的 Activity
if (activity.getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_NOSENSOR){
    //Do something
}
```
### 檢查系統螢幕旋轉
```java
int screenRot = 0;
//可能需要try catch
screenRot = Settings.System.getInt(activity.getContentResolver(), Settings.System.ACCELEROMETER_ROTATION);
if(screenRot == 1){} //系統有開自動旋轉
else //沒有自動旋轉
```

### 將檔案寫在非 root 無法拜訪的地方
```java
activity.getFilesDir();  // debug 時顯示路徑：/data/user/0/[project name]/files/
                         // 實際手機路徑：/data/data/[package name]/files/ 
```

try catch 注意事項:<br>
若有2個以上需要 try 的指令，需要開2個 try catch block。

## Setting
1. App name：`/app/src/main/AndroidManifest.xml`
```
<application
        ...
        android:label="@string/app_name"
        ...
        </activity>
    </application>
```

@string/app_name 設定：`/app/src/main/res/values/strings.xml`
```
<resources>
    <string name="app_name">name</string>
</resources>
```

2. 消除頂端預設bar：`/app/src/main/res/values/styles.xml`
```
<resources>
    <style name="AppTheme" parent="Theme.AppCompat.Light.NoActionBar">
</resources>
```

3. android 寫檔權限：`/app/src/main/AndroidManifest.xml`
```
  <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
```


## 打包 jar / aar
* [打包 jar 程式碼](#打包-jar-程式碼)
* [執行打包 jar 方法](#執行打包-jar-方法)
* [執行打包 aar 方法](#執行打包-aar-方法)

### 打包 jar 程式碼
jar 僅能打包 code

在 app 底下（或者跟 app 同階層，想要打包的 lib moudle 底下）的 build.gradle 中：

```
apply plugin: 'com.android.library'  //apply 用 library

android {

    ...

    sourceSets {
        main {
            java {
                srcDir 'src/main/'  //希望打包起來的 source code 路徑
            }
        }
    }
    defaultConfig {
        ...
    }
    buildTypes {
        release {
            minifyEnabled false
            proguardFiles getDefaultProguardFile('proguard-android-optimize.txt'), 'proguard-rules.pro'
        }
    }
    lintOptions {
        abortOnError false
    }
}

dependencies {
    ...
}

task deleteOldJar(type: Delete) {
    //刪掉舊的檔
    delete 'release/your_lib.jar'
}

//android studio 這裡左邊會有 play 鍵，直接按即可打包
task exportJar(type: Copy) {
    from('build/intermediates/packaged-classes/release/classes.jar')
    into('release/')
    include('classes.jar')

    rename('classes.jar', 'your_lib.jar')
}

exportJar.dependsOn(deleteOldJar, build)
```

### 執行打包 jar 方法
1. build.gradle 程式碼頁面中，task exportJar 左邊會有 play 鍵，直接按即可打包
2. android studio 右上角 Gradle 標籤中，找到要打包的包，展開找 exportjar，點兩下執行

output jar file:  打包的moudle路徑（Ex:app）/release/


### 執行打包 aar 方法
android studio 右上角 Gradle 標籤中，找到要打包的包，展開找 assembleRelease，點兩下執行

output aar file:  打包的moudle路徑（Ex:app）`/build/outputs/aar/`



## 匯入
* [匯入 jar](#匯入-jar)
* [匯入 aar](#匯入-aar)

### 匯入 jar
1. 將 jar 檔放入 libs 資料夾中
2. android studio 左上 Project 標籤中找到放入 libs 的 jar
3. 右鍵，點選 Add As Library


### 匯入 aar
1. 將 aar 檔放入 libs 資料夾中
2. app/build.gradle：

```
dependencies {
    implementation fileTree(include: ['*.jar'], dir: 'libs')
    implementation(name:'aar檔名', ext:'aar')
}

repositories{
    flatDir{
        dirs 'libs'
    }
}
```

## Android studio VM
使用模擬器生成檔案後取出方法：

`View` -> `Tool Windows` -> `Device File Explorer`



## 網路連線
* Android 網路連線功能不可在主 thread 執行，需開新 thread 連線。
  * 若需等待網路 thread 執行結束的結果，可用 `join`。
```java
Thread t;

// do something

t.join();

//這邊會等 t 做完才繼續做
```

* 遇到錯誤 `FileNotFoundException`：代表 404，可能網址寫錯

* Android 9 以上預設禁止使用 http，需自行開啟。<br>
新增檔案：`res/xml/network_security_config.xml`<br>
檔案內容：
```xml
<?xml version="1.0" encoding="utf-8"?>
<network-security-config>
    <domain-config cleartextTrafficPermitted="true">
        <domain includeSubdomains="true">52.185.174.41</domain>
    </domain-config>
</network-security-config>
```

AndroidManifest.xml 指定到剛剛新增的檔案:
```xml
<application android:networkSecurityConfig="@xml/network_security_config" />
```


## 改名

* 改 module name:<br>
  在 module 右鍵 Refactor -> Rename<br>
  選 Rename module (選到 Rename directory 會壞掉)<br>
* 改 package name:<br>
  專案顯示方式選 Android，取消勾選 Compact Middle Packages<br>
  對要改名的層級右鍵 Refactor -> Rename<br>
  選 Rename Package，輸入新名稱<br>
  下面按 Do Refactor<br>
  去 build.gradle 改 application<br>


## 存取設定資料

使用 `SharedPreferences`<br>

檔案存於：
```
/data/data/com.your.package.name/shared_prefs/[activity name].xml
/data/data/com.your.package.name/shared_prefs/[activity name].bak
```

建立 shared_prefs xml 檔案：<br>

Activity 有要用到很多個 xml：`getSharedPreferences()`<br>
Activity 只需要唯一的檔案，建立時不用自己命名：`getPreferences()`<br>


