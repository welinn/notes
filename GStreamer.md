## GStreamer

* [環境安裝](#環境安裝)
* [範例](#範例)
* [note](#note)



### 環境安裝

* docker:
```bash
docker run --gpus all -it --ipc=host --network=host --name gstreamer -e DISPLAY=$DISPLAY --device=/dev/ttyS0 -v /tmp/.X11-unix:/tmp/.X11-unix -v /etc/localtime:/etc/localtime:ro -v /home/user/Documents/:/workspace/ -w /workspace/ nvcr.io/nvidia/tensorrt:21.03-py3
```

* 常用的先裝起來
```bash
apt update
apt upgrade
apt install ffmpeg

pip install opencv-python
pip install ffmpeg-python
pip install cython
```

* [需要先安裝的套件](https://gstreamer.freedesktop.org/documentation/installing/on-linux.html?gi-language=c#install-gstreamer-on-ubuntu-or-debian)

```bash
apt install libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev libgstreamer-plugins-bad1.0-dev gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly gstreamer1.0-libav gstreamer1.0-tools gstreamer1.0-x gstreamer1.0-alsa gstreamer1.0-gl gstreamer1.0-gtk3 gstreamer1.0-qt5 gstreamer1.0-pulseaudio
```


### 範例
* 範例（from ChatGPT）
```python
import cv2
import numpy as np

import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst

# 初始化 GStreamer
Gst.init(None)

# 定義 GStreamer 流水線字符串
video_name = 'your_video.mp4'
pipeline_str = (
    f'filesrc location={video_name} ! decodebin ! videoconvert ! '
    'video/x-raw,format=BGR ! appsink name=app1'
)

rtsp_url = 'rtsp://XXX'
rtsp_pipeline_str = (
    f'rtspsrc location={rtsp_url} ! decodebin ! videoconvert ! '
    'video/x-raw,format=BGR ! appsink name=app1'
)


# 解析並創建 GStreamer 流水線
pipeline = Gst.parse_launch(pipeline_str)
#pipeline = Gst.parse_launch(rtsp_pipeline_str)

# 獲取 appsink 元件
appsink = pipeline.get_by_name('app1')

# 配置 appsink 以將數據作為 BGR 圖像提供
appsink.set_property("emit-signals", True)
appsink.set_property("sync", False)

# 開始播放
pipeline.set_state(Gst.State.PLAYING)


ctr = 0
# 事件循環
while True:
    # 等待新幀
    sample = appsink.emit("pull-sample")
    if sample:
        # 取得 GstBuffer 物件
        buffer = sample.get_buffer()
        # 轉換為 NumPy 陣列
        caps = sample.get_caps()
        width = caps.get_structure(0).get_int("width")[1]
        height = caps.get_structure(0).get_int("height")[1]

        # 創建 NumPy 陣列來存儲圖像
        np_array = np.ndarray(
            (height, width, 3), dtype=np.uint8, buffer=buffer.extract_dup(0, buffer.get_size())
        )

        if ctr < 10:
            cv2.imwrite(f'{ctr}.jpg', np_array)
            ctr += 1
        else:
            break


# 停止流水線並釋放資源
pipeline.set_state(Gst.State.NULL)
```

### note

* 檢查 gi 綁定：`python -c "import gi; print(gi.__version__)"`
* 檢查 version：`gst-inspect-1.0 --version`

#### 取 frame
* `emit-signals`： 是否使用 signals，預設 `False`
    因為效率問題，GStreamer 偏好使用 callback，因此 signals 預設為 `False`
    若要使用 `appsink.emit('pull-sample')`，必須設定 `appsink.set_property('emit-signals', True)`

* `pull_sample()`：[[doc](https://lazka.github.io/pgi-docs/GstApp-1.0/classes/AppSink.html#GstApp.AppSink.pull_sample)] 會 block，接串流時，解碼不夠快會爆記憶體
  * 版本：GstApp 1.0 (1.24.12.0)
  * 2025/02：apt install 版本：GStreamer 1.16.3，無法使用 `pull_sample()`
    ``` python
    #emit-signals 開啟時使用
    sample = appsink.emit("pull-sample")
    #可取代成以下
    sample = appsink.pull_sample()
    ```


