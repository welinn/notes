# Anaconda 筆記
* [安裝](#安裝)
* [指令](#指令)

## 安裝

到[**官網**](https://www.anaconda.com/distribution/)下載 bash 檔：
1. 在 Download 按鈕上按右鍵，複製連結網址
2. `wget [copied bash link]`
3. `./Anaconda3-XXXXXXX-Linux-x86_64.sh`
4. `sudo vim /etc/profile`
5. 添加路徑
```bash
export PATH="/home/username/anaconda3/bin:$PATH"
```
6. 使設定生效
```bash
source /etc/profile
```
7. 初始化
```bash
conda init
```

#### 安裝完後下 conda 無反應指令
```
source activate
source deactivate
```

#### 消除 Terminal (base) 字樣

```bash
conda config --set auto_activate_base false
```
此指令會產生 `.condarc` 檔案。


若上述方法無效，直接更改 .bashrc
```bash
sudo vim .bashrc
```
找出 anaconda 路徑設定部份
若有 if else 可先 `echo` 字串，在 terminal 中輸入 `source .bashrc`
確認開啟 terminal 時會進入哪行指令。
直接在該處加入：
```bash
conda deactivate
```


## 指令

建立環境：（可設定 python 版本）
```bash
conda create --name env_name python=3.6
```
複製環境：
```bash
conda create --name new_env_name --clone env_name
```
刪除環境：
```bash
conda env remove -n env_name
```
看有什麼版本：
```bash
conda search tensorflow-gpu
```
指令開 anaconda 圖形介面：
```bash
anaconda-navigator
```
