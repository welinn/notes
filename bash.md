### 檔名批次補0
* 把檔名中數字的前面補0
  * 有檔案 foo0, foo1, foo2, ..., foo9
  * 要改成 foo00, foo01, foo02, ..., foo09
```bash
for f in foo[0-9]*; do
  mv "$f" "$(printf 'foo%02d' "${f#foo}")"
done
```
* 改名後檔案副檔名會不見，補回副檔名
```bash
for f in *; do mv "$f" "$f.jpg"; done
```

### 只要移動資料夾裡面的100個檔案
```bash
for file in $(ls -p | grep -v / | tail -100)
do
  mv $file /other/location
done
```

### 圖片檔太大，分批轉 pdf
* mkdir 多個資料夾
```bash
for ((i=0;i<=15;i++)); do mkdir $i; done
```
* 把圖片分別放進資料夾後，各資料夾裡面圖片轉 pdf
```bash
for ((i=0;i<=15;i++)); do convert ${i}/*.png ${i}.pdf; done
```

### 滑鼠控制
* 將滑鼠移動到指定位置，每分鐘點一下
```bash
#!/bin/bash

#xdotool mousemove 2228 816
#xdotool click 1
xdotool mousemove 1824 978
for ((i=0;i<20;i++))
do
  sleep 60
  xdotool getmouselocation
  # xdotool mousemove 1824 978
  xdotool click 1
done
```

### ffmpeg 資料夾批次 mkv 轉 mp4
```bash
f="./*"
for path in ${f[@]}
do
  n=$(basename "$path")
  ffmpeg -i $path -r 20 ${n%.*}.mp4
done
```

### 撈 dpkg licenses
```bash
packages=`dpkg --get-selections | awk '{ print $1 }'`
for package in $packages; do echo "$package: "; cat /usr/share/doc/$package/copyright; echo ""; echo ""; done > ~/licenses.txt
```
