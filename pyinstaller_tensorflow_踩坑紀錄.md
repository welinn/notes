## pyinstaller

* `-F`: 將所有成使打包成一個執行檔
* `-p`: 有用到 sys.path.append 的路徑
* `--hidden-import`: 編譯完發現有東西沒包進去，加到這，重包

包成一個執行檔
```bash
pyinstaller -F main.py
```
包成資料夾，裡面有執行檔 + library
```bash
pyinstaller -D main.py
```
用 spec 包
```bash
pyinstaller main.spec
```

打包，qt 執行出錯：
```bash
pip uninstall pyqt5
conda install pyqt
```

### 坑
python json 要用 `"`, 不能用 `'`
python 傳 json 字串 `true`, `false` 要用小寫

### tensorflow 坑
tensorflow 2.x 版改了架構<br>
為了相容 1.x 版，架構在 `__init__` 裡面更改<br>
造成 pyinstaller 打包找不到 lib

[1.15, 2.2): 模組名稱改為 tensorflow_core<br>
2.2 ~: 模組名稱改回 tensorflow

實測安裝 pyinstaller version 5.0.dev0
```
pip install https://github.com/pyinstaller/pyinstaller/archive/develop.zip
```
在路徑 `/anaconda3/envs/env_name/lib/python3.8/site-packages/_pyinstaller_hooks_contrib/hooks/stdhooks`<br>
內有 `hook-tensorflow.py` 檔案<br>
裡面有對不同版本的處理


### 打包完後開 GPU 找不到 so 檔
cudnn 相關的 so 檔，因為不是直接在程式碼裡面 import，打包時不會連結到這些 library<br>
需要用 `--add-data=/path/for/so/file:/path/for/packaged`<br>
例如： `--add-data=/home/user/anaconda3/envs/env_name/lib/libcudart.so.10.1:./`<br>
其他需要的檔案，如 h5 檔、txt 檔等，一樣用 `--add-data` 帶入

也可直接寫在 spec 裡面，先打包一次之後就會自動生成，在裡面改就好
```python
a = Analysis(['main.py'],
             pathex=['/path/for/sourse/code'],
             binaries=[],
             datas=[
                 ('/home/user/anaconda3/envs/env_name/lib/libcudart.so.10.1', './'),
                 ('/home/user/anaconda3/envs/env_name/lib/libcublas.so.10', './'),
                 ('/home/user/anaconda3/envs/env_name/lib/libcufft.so.10', './'),
                 ('/home/user/anaconda3/envs/env_name/lib/libcurand.so.10', './'),
                 ('/home/user/anaconda3/envs/env_name/lib/libcusolver.so.10', './'),
                 ('/home/user/anaconda3/envs/env_name/lib/libcusparse.so.10', './'),
                 ('/home/user/anaconda3/envs/env_name/lib/libcublasLt.so.10', './'),
                 ('/home/user/anaconda3/envs/env_name/lib/libcudnn.so.7', './'),
                 ('path/for/model/model.h5', 'path/for/model'),
             ],

             ...

```
輸出的名稱可在 spec 裡面 `coll = COLLECT` 的 `name` 變數改


