* [ros 環境安裝](#ros-環境安裝)
  * [安裝](#安裝)
  * [測試 ros](#測試-ros)
* [相機校正工具](#相機校正工具)
  * [usb_cam](#usb_cam)
  * [camera_calibration](#camera_calibration)

## ros 環境安裝

### 安裝
[教學連結](http://wiki.ros.org/melodic/Installation/Ubuntu)

設定 sources.list
```bash
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
```

設定 key
```bash
curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | sudo apt-key add -
```

安裝<br>
ros 有多種版本可以選，這邊裝的是全部都有的版本 `ros-melodic-desktop-full`
```bash
sudo apt update
sudo apt install ros-melodic-desktop-full
```

設定環境變數
```bash
echo "source /opt/ros/melodic/setup.bash" >> ~/.bashrc
source ~/.bashrc
```

安裝其他相依套件
```bash
sudo apt install python-rosdep python-rosinstall python-rosinstall-generator python-wstool build-essential
```

初始化 `rosdep`
```bash
sudo rosdep init
rosdep update
```

### 測試 ros
跑 ros 的工具時，需要先開一個視窗把服務跑起來
```bash
roscore
```

開一個新的 terminal，跑起來會看到一隻海龜
```bash
rosrun turtlesim turtlesim_node
```

再開一個新的 terminal，在這個 terminal 用按鍵盤上下左右來操縱海龜
```bash
rosrun turtlesim turtle_teleop_key
```


## 相機校正工具
需要2個工具：
1. [usb_cam](http://wiki.ros.org/usb_cam)：相機的 driver
2. [camera_calibration](http://wiki.ros.org/camera_calibration/Tutorials/MonocularCalibration)：校正工具

#### usb_cam


driver 跑起來之後預設會開名稱為 /camera/image_raw 的 topic


#### camera_calibration
安裝
```bash
rosdep install camera_calibration
```

檢查是不是已經有把 camera driver 的 topic 跑起來了<br>
(要跑這個之前確認 ros 服務 `roscore` 有跑起來，不然會 error)
```bash
rostopic list
```

如果 driver 有起來，一般預設的 topic 會是：
```bash
/camera/camera_info
/camera/image_raw
```

把校正工具跑起來
```bash
rosrun camera_calibration cameracalibrator.py --size 8x6 --square 0.108 image:=/camera/image_raw camera:=/camera
```
* `size`： 校正板的尺寸是幾乘幾<br>
* `square`： 校正板每個格子的面積（平方公尺）<br>
* `image:=/camera/image_raw`：從 topic `/camera/image_raw` 取得攝影機的畫面
