**python ctypes 只能吃 C 的格式，C++ 要轉 C**

## C++ 部份
C++ class：
```cpp
class Cap{
  public:
    int w, h;
    Cap(String path){  //建構子
      ...
    }
    void foo(){  //function
      ...
    }
};

extern "C" {
  Cap* Cap_new(char* path){ return new Cap(path); }
  void Cap_foo(Cap* c){ return c->foo();}
  int getW(Cap* c){return c->w;}
  int getH(Cap* c){return c->h;}
}
```
C++ 的 `String` 在用 extern "C" 的時候要改成 `char*`<br>
物件的 function 在 extern "C" 時傳入物件本人，利用傳入的物件去呼叫函式<br>
編成 `.o` 檔給 python 用


編譯含 opencv 指令：
* opencv3 用 `opencv` 即可
* opencv4 用 `opencv4` (apt install 版)
  * apt 上的 opencv 已經更新到版本 4，可以直接裝了 `sudo apt install libopencv-dev`

cpp 編成 .o 檔：
```bash
g++ -c -fPIC cap.cpp `pkg-config opencv --cflags --libs` -o cap.o
g++ -c -fPIC cap.cpp `pkg-config --cflags --libs opencv4` -o cap.o
```

.o 編成 .so 檔：
```bash
g++ -shared -Wl,-soname,libcap.so -o libcap.so cap.o `pkg-config opencv --cflags --libs`
g++ -shared -Wl,-soname,libcap.so -o libcap.so cap.o `pkg-config --cflags --libs opencv4`
```

## Python 部份
python 在使用時，用 extern "C" 裡面的 function name：<br>
**抓 int 的部份怪怪的，得到的數字超大，可能有哪邊格式不對**
```python
import ctypes
from ctypes import cdll

lib = cdll.LoadLibrary('./libcap.so')  #剛剛編譯出來的 so 檔
lib.getW.restype = ctypes.c_int        #設定 function return 的型別
lib.getH.restype = ctypes.c_int
#function 有參數的話，有幾個參數就要設幾個 type，例如：
#lib.foo.argtypes = [ctypes.c_uint, ctypes.c_char_p, ctypes.c_int]

class Cap(object):
    def __init__(self, path):
        self.obj = lib.Cap_new(path)
        self.w = lib.getW()
        self.h = lib.getH()

    def foo(self):
        return lib.Cap_foo(self.obj)
```
