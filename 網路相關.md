有線網路消失，設定找不到
`ifconfig` eth0 不見
`ifconfig -a` 看到 `enp0s31f6`


先安裝 netplan：
```bash
sudo apt-get install netplan.io
```

看 netplan 裡面的 yaml 檔，例如：
```bash
cat /etc/netplan/01-network-manager-all.yaml
```
內容：
```
# Let NetworkManager manage all devices on this system                                                                               
network:
  version: 2
  renderer: NetworkManager

```

把設定加入檔案
```bash
sudo vim /etc/netplan/01-network-manager-all.yaml
```

設定內容，ip 最後的 24 代表 `255.255.255.0`：
```
network:
  version: 2
  renderer: NetworkManager

  ethernets:
    eth0:
      dhcp4: no
      addresses: [192.168.0.201/24]
      gateway4: 192.168.0.254
  wifis:
    wlp2s0:
      dhcp4: true
      optional: true
```

存檔後下指令，用 try 的話，如果沒問題他會倒數秒數問要不要沿用設定
```bash
sudo netplan try
```

### 設完後有線網路好了但無線網路死亡 QAQ [2021.12.29 未解重灌]
圖形介面設定中，wifi 打不開，有訊息：Wi-Fi Hardware Disabled

看全部的網卡有哪些
```bash
ls /sys/class/net/
```

這個指令可以看到 Wireless 被 disable
```bash
sudo lshw -C network
sudo lshw -class network
```

下這個指令看到 Hard blocked 是 yes
```bash
rfkill list all
```
但下 unblock 沒有效果
```bash
rfkill unblock all
```


但下這個指令，會得到 0<br>
代表正常，硬體沒有 disable
```bash
cat /sys/class/rfkill/rfkill0/hard 
```

下這個指令，得到 1
```bash
cat /sys/class/rfkill/rfkill0/state 
```


