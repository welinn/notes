* [雜七雜八](#雜七雜八)
* [正則表達式](#正則表達式)
* [ctrl-c](#ctrl-c)

## 雜七雜八
* 有陣列 p，取數值前3大的數字跟 index
```python
  heapq.nlargest(3, p)                                 #值
  heapq.nlargest(3, range(len(p)), key=p.__getitem__)  #index
```

* 前面沒有補0的數字檔名排序
```python
import re
imgs.sort(key=lambda f: int(re.sub('\D', '', f)))
```

* 看物件裡面有什麼 attribute
```python
obj.__dict__.keys()
```

* 檢查 box 有沒有重疊： X Y 方向都要 true
```python
def box_overlap_1D(min1, max1, min2, max2):
    return max1 >= min2 and max2 >= min1
```

* 直接執行指令
```python
os.system('xxxx')
```

* 迴圈抓某資料夾裡面全部.py檔 import class
```python
path = 'event/events'
for py in [f[:-3] for f in os.listdir(path) if f.endswith('.py') and f != '__init__.py']:
    import_str = f'from event.events.{py} import {py}'
    try:
        exec(import_str)
    except:
        pass
```

## 正則表達式

* 想要找被夾在某2個 label 之間的字串
  * `x(.*?)y`: 匹配「開頭x，結尾y」的字串
```python
import re 

a = 'abc"def"ghijk'
b = 'abcdefghijk'

pattern_1 = re.compile(r'\"(.*?)\"') 
pattern_2 = re.compile(r'cd(.*?)hi') 

sub_a = re.findall(pattern_1, a)
sub_b = re.findall(pattern_2, b)


#sub_a = 'def'
#sub_b = 'efg'
```

## ctrl-c 
* python 遇到 ctrl-c 先執行某些程式碼再關掉

使用套件 `signal`<br>
寫一個 function，把在遇到 `ctrl-c` 時要做的事寫在裏面<br>

```python
import signal

def handler(signum, frame):
    #這邊放要做的事

    sys.exit() #關閉程式

signal.signal(signal.SIGINT, handler)

```

有很多參數想傳進去 or 有很多不同條件，開起來的東西都不一樣，不想寫一堆 if else：<br>
把 handler function 放在變數存在的 block 裏面（例如 main）<br>
參數加上 `*args`，這樣 handler 裏面可以看到 main 的全部變數 <br>

```python
import signal

def main():
    ...

    def handler(*args):
        #這邊放要做的事
        sys.exit() #關閉程式
    signal.signal(signal.SIGINT, handler)
```


也可以在遇到 ctrl-c 時，問使用者是不是真的要關閉程式，[來源](https://code-maven.com/catch-control-c-in-python)<br>
```python
import signal

def handler(signum, frame):
    res = input("Ctrl-c was pressed. Do you really want to exit? y/n ")
    if res == 'y':
        exit(1)

signal.signal(signal.SIGINT, handler)
```
