# 開機執行設定
```
sudo vim /lib/systemd/system/rc.local.service
```
最下方加入
```
[Install]  
WantedBy=multi-user.target  
Alias=rc-local.service
```

建立`rc.local`檔案
```
sudo vim /etc/rc.local 
```

填入內容
```bash
#!/bin/sh -e
#
# rc.local
#
# This script is executed at the end of each multiuser runlevel.
# Make sure that the script will "exit 0" on success or any other
# value on error.
#
# In order to enable or disable this script just change the execution
# bits.
#
# By default this script does nothing.

#要執行的指令放這

#== 範例：執行 nohup ==
# 要 cd 的話，指令要括起來
# 指令 a && b 表 a 執行成功才會執行 b
# 指令 a ; b 表無論如何執行 a 和 b
# > 表覆寫，想用新增方式用 >>
# 背景執行後面 + &
(cd /path/for/file/ && nohup node ex.js &> /path/for/log/nohup.out) &

exit 0
```

改權限
```
chmod 755 /etc/rc.local 
```

設定連結
```
ln -s /lib/systemd/system/rc.local.service /etc/systemd/system/
```
