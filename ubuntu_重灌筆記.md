## 重灌筆記 
* [1. 先裝起來](#1-先裝起來)
* [2. 中文輸入法](#2-中文輸入法)
* [3. 顯卡驅動](#3-顯卡驅動)
* [4. docker](#4-docker)
* [5. 其他](#5-其他)
* [6. 疑難雜症](#6-疑難雜症)
  * [Sound Switcher Indicator](#sound-switcher-indicator)
  * [wayland vs xorg](#wayland-vs-xorg)

### 1. 先裝起來
```bash
sudo apt update
sudo apt upgrade
sudo apt install ffmpeg vim git curl g++ xdotool gstreamer1.0-libav
```
* GNOME Tweaks
* Extension Manager：調時間日期格式用
  * Date Menu Formatter

### 2. 中文輸入法
* 22.04 裝 gcin 會壞掉，改用 fcitx5

```bash
sudo apt purge fcitx*              #移除舊版
sudo apt install fcitx5 fcitx5-*
```
* 加裝 [小麥輸入法](https://github.com/openvanilla/fcitx5-mcbopomofo)
* 要 cmake 記得裝 g++

```bash
sudo apt install libfcitx5core-dev libfcitx5config-dev libfcitx5utils-dev cmake extra-cmake-modules gettext libfmt-dev
```

* fcitx5 configure 內：
  * Chewing：新酷音
  * mcbopomofo：小麥
  * Anthy：日文

### 3. 顯卡驅動
* 關 X window
```bash
sudo systemctl set-default multi-user.target
```
* nvidia 新版 driver 有選項可以幫忙關 nouveau，不用自己來
* 打開 X window
```bash
sudo systemctl set-default graphical.target
```


### 4. docker
* docker 直接用 sh 安裝
```bash
curl -fsSL https://get.docker.com/ | sh
sudo chmod a+rw /var/run/docker.sock
```
* 其他套件
```bash
xhost +local:
distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add -
curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list
sudo apt update && sudo apt-get install -y nvidia-docker2
sudo systemctl restart docker
```
#### docker 裡安裝 opencv
```bash
pip install opencv-python
```
* 出現 error
```bash
ImportError: libGL.so.1: cannot open shared object file: No such file or directory
```
* 另外安裝
```bash
apt update
apt install -y libgl1-mesa-dev
```

### 5. 其他
* show application 移到頂端
```bash
gsettings set org.gnome.shell.extensions.dash-to-dock show-apps-at-top true
```

* 軟體中心：
  * kazam
  * kolourPaint
  * gimp
  * calibre
  * netron

* 指令安裝
  * pdftk
  * convert：記得把禁用PDF註解掉
    ```bash
    sudo vim /etc/ImageMagick-6/policy.xml
    ```
    註解這行
    ```xml
    <policy domain="coder" rights="none" pattern="PDF" />
    ```
  * tg
  ```bash
  sudo snap install pdftk
  sudo apt install imagemagick telegram-desktop
  ```

* 載 deb
  * FortiClient

* webtoon downloader [連結](https://github.com/Zehina/Webtoon-Downloader)
```bash
pip install webtoon_downloader
```

#### vakten

* 用指令裝才看得到缺什麼套件
```bash
sudo dpkg -i InstallVakten.deb
```

* 缺的
```bash
#libgdk-pixbuf2.0-0
sudo apt install libgdk-pixbuf2.0-0

#libpng12-0 無法直接載到
sudo apt-get install software-properties-common
sudo apt-get update
sudo add-apt-repository ppa:linuxuprising/libpng12
sudo apt update
sudo apt install libpng12-0

#啟動後可能會缺的
sudo apt install libcanberra-gtk-module libcanberra-gtk3-module
```

### 6. 疑難雜症

#### Sound Switcher Indicator
* 聲音會被 HDMI 搶走，裝這個套件方便選擇哪邊輸出
* 連結
  * [github](https://github.com/yktoo/indicator-sound-switcher)
  * [網站](https://yktoo.com/en/software/sound-switcher-indicator/installation/)

**安裝方法**
1. PPA (recommended)
```bash
sudo apt-add-repository ppa:yktooo/ppa
sudo apt update
sudo apt install indicator-sound-switcher
```

2. Snap package
```bash
sudo snap install indicator-sound-switcher
```

#### wayland vs xorg
**ffmpeg 錄影黑屏**
* `echo $XDG_SESSION_TYPE` 檢查看看現在桌面出是不是 `wayland`
* 如果是的話：
  * `sudo vim /etc/gdm3/custom.conf`
  * 註解可以不開，到時候在 user 登入畫面可以選要用哪種登入
  ```bash
  #WaylandEnable=false
  DefaultSession=gnome-xorg.desktop
  ```
  * 重新開機，登入時齒輪點開選 Ubuntu on Xorg



