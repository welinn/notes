# Vim

* [設定教學](http://wiki.csie.ncku.edu.tw/vim/vimrc)
* [set](#set)
* [「:」指令](#指令)
* [搜尋](#搜尋)

### set

* `set nu`：顯示行號
* `set tabstop=4`：tab 空白4格
* `set shiftwidth=4`：自動縮排對齊間隔數：向右或向左一個縮排的寬度
* `set expandtab`：把 tab 改成空白
* `set cursorline`：游標所在行有底線



* **避免貼上時自動縮排**
```
:set paste
```
設完後 `i` 進去會看到 `-- INSERT (paste) --`<br>
貼完之後
```
:set nopaste
```
`i` 就會變回 `-- INSERT --`

### 「:」指令

* 對全域(`g`) 不符合(`!`) 開頭是(`^`) +- 的行進行刪除(`d`)
```
:g!/^[+-]/d
```

* 對全域(`g`) 不符合(`!`) 開頭是(`^`) a 的行進行刪除(`d`)
```
:g!/^a/d
```

### 搜尋

**找用 `v` 模式下選起來的文字**<br>
選起來之後：

1. `y`： 複製
2. `/`： 進搜尋
3. `\V`： 有特殊字元時要加(very no magic mode)（可選）
4. `Ctrl` + `r`： 跟 vim 說我要貼上
5. `"` 或 `0`： 貼上 `y` 裡複製的字
6. 有反斜線要自己再補反斜線，換行要從 `^M` 改 `\n`


* 搜尋文字時有特殊字元：使用 magic mode
  * `\m`：magic mode
  * `\M`：no magic mode
  * `\v`：very magic mode
  * `\V`：very no magic mode -> 最泛用，除了 `\`，其他都會視為一般文字
* 4 種 magic mode 都不能直接吃 `\`，要手打補成 `\\`


